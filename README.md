# GitLab CI template for k6 loading test

This project implements a GitLab CI/CD template to run your automated load-testing with [k6](https://k6.io/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/k6/gitlab-ci-k6@3.4.0
    # 2: set/override component inputs
    inputs:
      review-enabled: true # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/k6'
    ref: '3.4.0'
    file: '/templates/gitlab-ci-k6.yml'

variables:
  # 2: set/override template variables
  REVIEW_ENABLED: "true" # ⚠ this is only an example
```

:warning: depending on your needs and environment, you might have to use [one of the template variants](#variants).

## `k6` job

This job starts [k6](https://k6.io/) tests.

It uses the following variables:

| Input / Variable | Description                                                                                               | Default value          |
| ------------------- | --------------------------------------------------------------------------------------------------------- | ---------------------- |
| `image` / `K6_IMAGE` | The Docker image used to run k6                                                                           | `registry.hub.docker.com/loadimpact/k6:latest` |
| `tests-dir` / `K6_TESTS_DIR` | The k6 tests directory                                                                             | `k6`                    |
| `extra-args` / `K6_EXTRA_ARGS` | k6 extra  [command-line](https://k6.io/docs/getting-started/running-k6)                                   | _none_                 |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable k6 tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled)      |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/k6-*.summary.json` | [k6 summary](https://k6.io/docs/using-k6/k6-options/reference/#summary-export) report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsload_performance) |
| `reports/k6-*.native.json` | [k6 JSON](https://k6.io/docs/results-visualization/json/) detailed stats | - |

### Load performance report integration

k6 test reports are [integrated to GitLab by generating load performance reports](https://docs.gitlab.com/ee/ci/testing/load_performance_testing.html).

This is done using the following CLI options: `--out json=reports/`  

### Base URL auto evaluation

By default, the k6 template tries to auto-evaluate the base URL (i.e. the variable pointing at server under test) by
looking either for a `$environment_url` variable or for an `environment_url.txt` file.

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the k6 test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your k6 tests.

In order to use the auto-evaluated base URL, you shall use the `base_url` environment variable from your k6 scripts.

Example:

```javascript
import http from 'k6/http';
import {check, sleep} from 'k6';

export default function() {
  const data = {username: 'username', password: 'password'};
  let res = http.post(__ENV.base_url, data);
    check(res, { 'success login': (r) => r.status === 200 });
    sleep(0.3);
}
```
